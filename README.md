# System wypożyczalni książek

## Opis biznesowy

System w założeniu ma być kompletnym rozwiązaniem potrzeb informatyzacji działania biblioteki. Ma pozwalać na rezerwację książek, zakładanie karty bibliotecznej, przejrzenie statusu książek oraz zarządzanie klientami biblioteki (należności pieniężne, sprawdzenie listy wypożyczeń). Tym samym zdejmuje z bibliotekarza część obowiązków. Dzięki temu pozwala to na lepszą organizację pracy oraz zmniejsza szansę na pomyłkę lub błąd wynikający z czynnika ludzkiego. Największym stresem nowych pracowników każdej branży jest obsługa finansowa klienta. System zdejmuje z nich ten stres i sam oblicza konieczne płatności danego użytkownika biblioteki.

Nasz plan projektowy w obecnej formie nie przewiduje wprowadzenia obsługi filli bilbiotecznych, ograniczając złożoność przetwarzanych danych. 

### Funkcjonalności

1. Wypożyczanie książek - system umożliwia wypożyczenie książek danemu użytkownikowy na określony czas, oraz nalicza należność za przekroczenie terminu oddania.
   - Rezerwacja ksiązek - system umożiwia rezerwowanie książek danemu użytkownikowi
   - Przedłużanie książek - system umożliwia przesunięcie terminu oddawania książki z podanym powodem
2. Tworzenie użytkowników - zakładanie karty bibliotecznej przez pracownika biblioteki.
   - Zarządzanie kontami pracowników i użytkowników biblioteki.
3. Zarządzanie zbiorem książek
   - Kategoryzacja książek
   - Oznaczanie książęk pod względem długości możliwego wypożyczenia
   - Dodawanie, edytowanie oraz usuwanie książki z zbioru
   - Zarządzanie ilością posiadanych książek
4. Profil użytkownika
   - Edycja danych użytkownika
   - Wyświetlenie listy wypożyczeń (obecne i przeszłe)
   - Sprawdzenie obecnej należności wobec biblioteki

### Technologie

- Backend: node.js/go, postgresql
- Frontend: html, scss, js
